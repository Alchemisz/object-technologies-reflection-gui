import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContainerComponent } from '../main-container/main-container.component';
import { ObjectInstanceDetailsComponent } from '../main-container/main-content/object-instance-details/object-instance-details.component';
import { CreateInscanceComponent } from '../main-container/main-content/create-inscance/create-inscance.component';

const appRoutes: Routes = [
  {
    path: '',
    component: MainContainerComponent,
    children: [
      {
        path: 'object-instances/details',
        component: ObjectInstanceDetailsComponent,
      },
      {
        path: 'available-classes/create-instance',
        component: CreateInscanceComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
