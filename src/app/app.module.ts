import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { MainContentComponent } from './main-container/main-content/main-content.component';
import { AvailableClassesComponent } from './main-container/available-classes/available-classes.component';
import { ObjectInstancesComponent } from './main-container/object-instances/object-instances.component';
import { HttpClientModule } from '@angular/common/http';
import { ObjectInstanceDetailsComponent } from './main-container/main-content/object-instance-details/object-instance-details.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { CreateInscanceComponent } from './main-container/main-content/create-inscance/create-inscance.component';
import { FormsModule } from '@angular/forms';
import { ClassesConfigurationComponent } from './main-container/available-classes/classes-configuration/classes-configuration.component';
import { NgToastModule } from 'ng-angular-popup';

@NgModule({
  declarations: [
    AppComponent,
    MainContainerComponent,
    MainContentComponent,
    AvailableClassesComponent,
    ObjectInstancesComponent,
    ObjectInstanceDetailsComponent,
    CreateInscanceComponent,
    ClassesConfigurationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgToastModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
