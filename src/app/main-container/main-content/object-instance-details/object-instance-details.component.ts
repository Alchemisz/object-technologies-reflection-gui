import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  AutocompleteView,
  AutocompleteViews,
  FieldView,
  MethodView,
  ObjectInstanceView,
  ObjectInstancesService,
} from '../../object-instances/object-instances.service';
import { NgForm } from '@angular/forms';
import { TypesHelper } from 'src/app/shared/types.helper';

@Component({
  selector: 'app-object-instance-details',
  templateUrl: './object-instance-details.component.html',
  styleUrls: ['./object-instance-details.component.css'],
})
export class ObjectInstanceDetailsComponent implements OnDestroy {
  objectInstanceSubscription: Subscription = new Subscription();
  objectInstance!: ObjectInstanceView;

  classMethodsSubscription: Subscription = new Subscription();
  classMethodsViews!: MethodView[];

  instanceFieldsSubscription: Subscription = new Subscription();
  instanceFieldsViews!: FieldView[];

  autocomplateSubscription: Subscription = new Subscription();
  autocomplateViewsByClassName: Map<String, AutocompleteView[]> = new Map();

  selectedMethodSignature: string = '';
  selectedMethodName!: string;
  activeMethodParams: string[] = [];

  selectedFieldSignature: string = '';
  selectedFieldName!: string;
  activeFieldParam!: string;

  objectInstanceFormFieldNames: Set<string> = new Set();

  isFieldBooleanTypeChecked: Map<String, Boolean> = new Map();

  constructor(private objectInstanceService: ObjectInstancesService) {
    this.objectInstanceSubscription =
      this.objectInstanceService.selectedInstanceSubject.subscribe((data) => {
        this.objectInstance = data;
      });

    this.classMethodsSubscription =
      this.objectInstanceService.methodsSubject.subscribe(
        (data) => (this.classMethodsViews = data)
      );

    this.instanceFieldsSubscription =
      this.objectInstanceService.fieldsSubject.subscribe((data) => {
        this.instanceFieldsViews = data;

        data
          .filter((field) => this.isBoolean(field.type))
          .forEach((field) =>
            this.isFieldBooleanTypeChecked.set(
              field.name,
              field.value === 'true'
            )
          );
      });

    this.autocomplateSubscription =
      this.objectInstanceService.autocompleteSubject.subscribe((data) =>
        data.map((autocompleteView) =>
          this.autocomplateViewsByClassName.set(
            autocompleteView.className,
            autocompleteView.views
          )
        )
      );
  }

  onInvokeMethod(formRef: NgForm) {
    let convertedMethodParams: any[] = [];

    for (let i = 0; i < this.activeMethodParams.length; i++) {
      if (this.activeMethodParams[i] === 'java.lang.Integer') {
        convertedMethodParams.push(parseInt(formRef.value['param' + (i + 1)]));
      } else if (this.activeMethodParams[i] === 'java.lang.String') {
        convertedMethodParams.push(formRef.value['param' + (i + 1)]);
      } else {
        this.objectInstanceFormFieldNames.has('param' + (i + 1))
          ? convertedMethodParams.push({
              objectInstanceId: formRef.value['param' + (i + 1)],
            })
          : convertedMethodParams.push(formRef.value['param' + (i + 1)]);
      }
    }

    console.log('======CONVERTED========');
    console.log(convertedMethodParams);
    console.log('====================================');

    this.objectInstanceService.invokeMethod(
      this.selectedMethodName,
      this.objectInstance.id,
      convertedMethodParams
    );
    this.objectInstanceService.refreshObjectInstances(this.objectInstance.id);
  }

  onFieldSet(formRef: NgForm) {
    let convertedFieldParam: any;

    if (this.activeFieldParam === 'java.lang.Integer') {
      convertedFieldParam = parseInt(formRef.value[this.selectedFieldName]);
    } else if (this.activeFieldParam === 'java.lang.String') {
      convertedFieldParam = formRef.value[this.selectedFieldName];
    } else if (this.activeFieldParam === 'java.lang.Boolean') {
      convertedFieldParam = this.isFieldBooleanTypeChecked.has(
        this.selectedFieldName
      )
        ? this.isFieldBooleanTypeChecked.get(this.selectedFieldName)
        : false;
    } else {
      if (this.objectInstanceFormFieldNames.has(this.activeFieldParam)) {
        convertedFieldParam = {
          objectInstanceId: formRef.value[this.selectedFieldName],
        };
      } else {
        convertedFieldParam = formRef.value[this.selectedFieldName];
      }
    }

    console.log('======CONVERTED========');
    console.log(convertedFieldParam);
    console.log('====================================');

    this.objectInstanceService.setField(
      this.selectedFieldName,
      this.objectInstance.id,
      convertedFieldParam
    );
    this.objectInstanceService.refreshObjectInstances(this.objectInstance.id);
  }

  setActiveMethod(methodView: MethodView) {
    this.selectedFieldSignature = '';
    this.selectedMethodName = methodView.name;
    this.selectedMethodSignature = methodView.value;
    this.activeMethodParams = methodView.paramsTypesNames;

    let notStandardParamsNames = this.activeMethodParams.filter(
      (paramName) => !this.isStandardType(paramName)
    );

    this.objectInstanceService.getObjectInstancesByClassNames(
      notStandardParamsNames
    );
  }

  setActiveField(fieldView: FieldView) {
    this.selectedMethodSignature = '';
    this.selectedFieldName = fieldView.name;
    this.selectedFieldSignature =
      fieldView.modifier + ' ' + fieldView.simpleType + ' ' + fieldView.name;
    this.activeFieldParam = fieldView.type;

    if (!this.isStandardType(this.activeFieldParam)) {
      this.objectInstanceService.getObjectInstancesByClassNames([
        this.activeFieldParam,
      ]);
    }
  }

  onCheckboxChange(paramName: string) {
    if (!this.isFieldBooleanTypeChecked.has(paramName)) {
      this.isFieldBooleanTypeChecked.set(paramName, true);
      return;
    }

    this.isFieldBooleanTypeChecked.set(
      paramName,
      !this.isFieldBooleanTypeChecked.get(paramName)
    );
  }

  getFieldType(typeName: string): string {
    return TypesHelper.getFieldType(typeName);
  }

  isStandardType(typeName: string): boolean {
    return TypesHelper.isStandardType(typeName);
  }

  isBoolean(typeName: string): boolean {
    return TypesHelper.isBoolean(typeName);
  }

  changeInstanceFormFieldNames(paramName: string) {
    this.objectInstanceFormFieldNames.add(paramName);
    console.log(this.objectInstanceFormFieldNames);
  }

  getForAutocomplete(
    activeMethodParam: string
  ): AutocompleteView[] | undefined {
    return this.autocomplateViewsByClassName.get(activeMethodParam);
  }

  ngOnDestroy(): void {
    this.objectInstanceSubscription.unsubscribe();
    this.classMethodsSubscription.unsubscribe();
    this.instanceFieldsSubscription.unsubscribe();
    this.autocomplateSubscription.unsubscribe();
  }
}
