import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, of } from 'rxjs';
import {
  AvailableClassesService,
  ConstructorView,
} from '../../available-classes/available-classes.service';
import {
  AutocompleteView,
  ObjectInstancesService,
} from '../../object-instances/object-instances.service';
import { NgForm } from '@angular/forms';
import { TypesHelper } from 'src/app/shared/types.helper';

@Component({
  selector: 'app-create-inscance',
  templateUrl: './create-inscance.component.html',
  styleUrls: ['./create-inscance.component.css'],
})
export class CreateInscanceComponent implements OnDestroy {
  activeClassSubscription: Subscription = new Subscription();
  activeClass!: string;

  classConstructorsSubscription: Subscription = new Subscription();
  classConstructorsViews!: ConstructorView[];

  selectedConstructorSignature: string = '';
  activeConstructorParams: string[] = [];

  autocomplateSubscription: Subscription = new Subscription();
  autocomplateViewsByClassName: Map<String, AutocompleteView[]> = new Map();

  objectInstanceFormFieldNames: Set<string> = new Set();

  isBooleanTypeChecked: Map<String, Boolean> = new Map();

  constructor(
    private availableClassesService: AvailableClassesService,
    private objectInstancesService: ObjectInstancesService
  ) {
    this.activeClassSubscription =
      this.availableClassesService.activeClassSubject.subscribe((data) => {
        this.activeConstructorParams = [];
        this.activeClass = data;
      });

    this.classConstructorsSubscription =
      this.availableClassesService.constructorsSubject.subscribe(
        (data) => (this.classConstructorsViews = data)
      );

    this.autocomplateSubscription =
      this.objectInstancesService.autocompleteSubject.subscribe((data) =>
        data.map((autocompleteView) =>
          this.autocomplateViewsByClassName.set(
            autocompleteView.className,
            autocompleteView.views
          )
        )
      );
  }

  setActiveConstrucor(
    constructorSignature: string,
    constructorParams: string[]
  ) {
    this.selectedConstructorSignature = constructorSignature;
    this.activeConstructorParams = constructorParams;

    let notStandardParamsNames = this.activeConstructorParams.filter(
      (paramName) => !this.isStandardType(paramName)
    );

    this.objectInstancesService.getObjectInstancesByClassNames(
      notStandardParamsNames
    );
  }

  onCreateInstance(formRef: NgForm) {
    let convertedConstructorParams: any[] = [];

    console.log(formRef.value);

    for (let i = 0; i < this.activeConstructorParams.length; i++) {
      if (this.activeConstructorParams[i] === 'java.lang.Integer') {
        convertedConstructorParams.push(
          parseInt(formRef.value['param' + (i + 1)])
        );
      } else if (this.activeConstructorParams[i] === 'java.lang.String') {
        convertedConstructorParams.push(formRef.value['param' + (i + 1)]);
      } else if (this.activeConstructorParams[i] === 'java.lang.Boolean') {
        this.isBooleanTypeChecked.has('param' + (i + 1))
          ? convertedConstructorParams.push(
              this.isBooleanTypeChecked.get('param' + (i + 1))
            )
          : convertedConstructorParams.push(false);
      } else {
        this.objectInstanceFormFieldNames.has('param' + (i + 1))
          ? convertedConstructorParams.push({
              objectInstanceId: formRef.value['param' + (i + 1)],
            })
          : convertedConstructorParams.push(formRef.value['param' + (i + 1)]);
      }
    }

    console.log('======CONVERTED========');
    console.log(convertedConstructorParams);
    console.log('====================================');

    console.log(this.activeClass);

    this.objectInstancesService.createInstanceForClassAndParameters(
      this.activeClass,
      convertedConstructorParams
    );
  }

  onCheckboxChange(paramName: string) {
    if (!this.isBooleanTypeChecked.has(paramName)) {
      this.isBooleanTypeChecked.set(paramName, true);
      return;
    }

    this.isBooleanTypeChecked.set(
      paramName,
      !this.isBooleanTypeChecked.get(paramName)
    );
  }

  changeInstanceFormFieldNames(paramName: string) {
    this.objectInstanceFormFieldNames.add(paramName);
    console.log(this.objectInstanceFormFieldNames);
  }

  isStandardType(typeName: string): boolean {
    return TypesHelper.isStandardType(typeName);
  }

  getFieldType(typeName: string): string {
    return TypesHelper.getFieldType(typeName);
  }

  isBoolean(typeName: string): boolean {
    return TypesHelper.isBoolean(typeName);
  }

  ngOnDestroy(): void {
    this.activeClassSubscription.unsubscribe();
    this.autocomplateSubscription.unsubscribe();
  }
}
