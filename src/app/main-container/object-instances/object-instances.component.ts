import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ObjectInstanceView,
  ObjectInstancesService,
} from './object-instances.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-object-instances',
  templateUrl: './object-instances.component.html',
  styleUrls: ['./object-instances.component.css'],
})
export class ObjectInstancesComponent implements OnInit, OnDestroy {
  objectInstancesSubscription: Subscription = new Subscription();
  objectInstances!: ObjectInstanceView[];

  constructor(
    private objectInstancesService: ObjectInstancesService,
    private router: Router
  ) {
    this.objectInstancesSubscription =
      this.objectInstancesService.objectInstancesSubject.subscribe(
        (data) => (this.objectInstances = data)
      );

    this.objectInstancesService.getObjectInstances();
  }

  preview(objectInstance: ObjectInstanceView) {
    this.objectInstancesService.changeObjectInstance(objectInstance);
    this.objectInstancesService.getFieldsByObjectInstanceId(objectInstance.id);
    this.objectInstancesService.getMethodsByClassName(objectInstance.className);
    this.router.navigate(['/object-instances/details']);
  }

  ngOnDestroy(): void {
    this.objectInstancesSubscription.unsubscribe();
  }

  ngOnInit(): void {}
}
