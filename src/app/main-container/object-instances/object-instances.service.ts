import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ToastService } from 'src/app/shared/toast.service';

export interface AutocompleteViews {
  className: string;
  views: AutocompleteView[];
}

export interface AutocompleteView {
  objectInstanceId: string;
  toStringValue: string;
}

export interface ObjectInstanceView {
  id: string;
  className: string;
  stringRepresentation: string;
}

export interface MethodView {
  value: string;
  name: string;
  paramsTypesNames: string[];
}

export interface FieldView {
  modifier: string;
  type: string;
  simpleType: string;
  name: string;
  value: string;
}

@Injectable({
  providedIn: 'root',
})
export class ObjectInstancesService {
  objectInstancesSubject: Subject<ObjectInstanceView[]> = new Subject<
    ObjectInstanceView[]
  >();

  methodsSubject: Subject<MethodView[]> = new Subject<MethodView[]>();

  fieldsSubject: Subject<FieldView[]> = new Subject<FieldView[]>();

  selectedInstanceSubject: Subject<ObjectInstanceView> =
    new BehaviorSubject<ObjectInstanceView>({
      id: '1',
      className: 'test',
      stringRepresentation: 'test',
    });

  autocompleteSubject: Subject<AutocompleteViews[]> = new Subject<
    AutocompleteViews[]
  >();

  constructor(private http: HttpClient, private toastService: ToastService) {}

  public getObjectInstances() {
    this.http
      .get<ObjectInstanceView[]>('http://localhost:8080/api/object-instances')
      .subscribe((response) => {
        console.log('====================================');
        console.log('REALOAD OBJECT INSTANCES');
        console.log('====================================');
        this.objectInstancesSubject.next(response);
      });
  }

  public createInstanceForClassAndParameters(
    className: string,
    parameters: any
  ) {
    this.http
      .post('http://localhost:8080/api/object-instances/create', {
        className: className,
        params: parameters,
      })
      .subscribe(
        () => {
          this.getObjectInstances();
          this.toastService.success('Utworzono instancje!');
        },
        (error) => this.toastService.error(error.error.text)
      );
  }

  public getMethodsByClassName(className: string) {
    this.http
      .get<MethodView[]>(
        'http://localhost:8080/api/classes/' + className + '/methods'
      )
      .subscribe((response) => this.methodsSubject.next(response));
  }

  public getFieldsByObjectInstanceId(objectInstanceId: string) {
    this.http
      .get<FieldView[]>(
        'http://localhost:8080/api/object-instances/' +
          objectInstanceId +
          '/fields/get'
      )
      .subscribe((response) => {
        this.fieldsSubject.next(response);
      });
  }

  invokeMethod(
    methodName: string,
    objectInstanceId: string,
    parameters: any[]
  ) {
    this.http
      .patch(
        'http://localhost:8080/api/object-instances/' +
          objectInstanceId +
          '/methods/invoke',
        {
          methodName: methodName,
          params: parameters,
        }
      )
      .subscribe(
        () => {
          this.getObjectInstances();
          this.getFieldsByObjectInstanceId(objectInstanceId);
          this.refreshObjectInstances(objectInstanceId);
          this.toastService.success('Wyłwoano metodę!');
        },
        (error) => this.toastService.error(error.error.text)
      );
  }

  setField(fieldName: string, objectInstanceId: string, value: any) {
    this.http
      .patch(
        'http://localhost:8080/api/object-instances/' +
          objectInstanceId +
          '/fields/set',
        {
          fieldName: fieldName,
          value: value,
        }
      )
      .subscribe(
        () => {
          this.getObjectInstances();
          this.getFieldsByObjectInstanceId(objectInstanceId);
          this.refreshObjectInstances(objectInstanceId);
          this.toastService.success('Ustawiono pole!');
        },
        (error) => this.toastService.error(error.error.text)
      );
  }

  public refreshObjectInstances(objectInstanceId: string) {
    this.http
      .get<ObjectInstanceView>(
        'http://localhost:8080/api/object-instances/' + objectInstanceId
      )
      .subscribe((response) => {
        console.log('====================================');
        console.log('REALOAD OBJECT INSTANCE ' + objectInstanceId);
        console.log('====================================');
        this.selectedInstanceSubject.next(response);
      });
  }

  public getObjectInstancesByClassNames(classNames: string[]) {
    this.http
      .post<AutocompleteViews[]>(
        'http://localhost:8080/api/object-instances/autocomplete',
        {
          classNames: classNames,
        }
      )
      .subscribe((response) => {
        console.log('====================================');
        console.log('AUTOCOMPLETE ' + classNames);
        console.log(response);
        console.log('====================================');
        this.autocompleteSubject.next(response);
      });
  }

  public changeObjectInstance(objectInstanceView: ObjectInstanceView) {
    this.selectedInstanceSubject.next(objectInstanceView);
  }
}
