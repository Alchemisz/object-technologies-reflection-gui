import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  ClassConfiguration,
  ClassesConfigurationService,
} from './classes-configuration.service';
import { AvailableClassesService } from '../available-classes.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-classes-configuration',
  templateUrl: './classes-configuration.component.html',
  styleUrls: ['./classes-configuration.component.css'],
})
export class ClassesConfigurationComponent implements OnInit {
  classesConfigurationSubscription: Subscription = new Subscription();
  classesConfiguration!: ClassConfiguration[];

  constructor(
    private classesConfigurationService: ClassesConfigurationService,
    private availabaleClassesService: AvailableClassesService
  ) {
    this.classesConfigurationSubscription =
      this.classesConfigurationService.classesConfigurationSubject.subscribe(
        (data) => (this.classesConfiguration = data)
      );

    this.classesConfigurationService.getClassesConfiguration();
  }

  onConfigurationChange(classConfiguration: ClassConfiguration) {
    this.classesConfigurationService.changeClassesConfiguration({
      className: classConfiguration.className,
      active: !classConfiguration.active,
    });
  }

  onAddNewClass(addClassForm: NgForm) {
    this.classesConfigurationService.addClassConfiguration({
      className: addClassForm.value['className'],
    });
  }

  ngOnInit(): void {}
}
