import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AvailableClassesService } from '../available-classes.service';
import { ObjectInstancesService } from '../../object-instances/object-instances.service';
import { ToastService } from 'src/app/shared/toast.service';

export interface ClassConfiguration {
  className: string;
  active: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class ClassesConfigurationService {
  classesConfigurationSubject: Subject<ClassConfiguration[]> = new Subject<
    ClassConfiguration[]
  >();

  constructor(
    private http: HttpClient,
    private availabaleClassesService: AvailableClassesService,
    private objectInstancesService: ObjectInstancesService,
    private toastService: ToastService
  ) {}

  public getClassesConfiguration() {
    this.http
      .get<ClassConfiguration[]>(
        'http://localhost:8080/api/class-configurations'
      )
      .subscribe((response) => this.classesConfigurationSubject.next(response));
  }

  public changeClassesConfiguration(command: any) {
    this.http
      .patch('http://localhost:8080/api/class-configurations/change', command)
      .subscribe((response) => {
        this.getClassesConfiguration();
        this.availabaleClassesService.getAvailableClasses();
        this.objectInstancesService.getObjectInstances();
      });
  }

  public addClassConfiguration(command: any) {
    this.http
      .post('http://localhost:8080/api/class-configurations/add', command)
      .subscribe(
        (response) => {
          this.getClassesConfiguration();
          this.availabaleClassesService.getAvailableClasses();
          this.toastService.success('Dodano klasę!');
        },
        (error) => {
          this.toastService.error(error.error.text);
        }
      );
  }
}
