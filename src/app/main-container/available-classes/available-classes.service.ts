import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject } from 'rxjs';

export interface ConstructorView {
  value: string;
  paramsTypesNames: string[];
}

@Injectable({
  providedIn: 'root',
})
export class AvailableClassesService {
  availableClassesSubject: Subject<string[]> = new Subject<string[]>();
  constructorsSubject: Subject<ConstructorView[]> = new Subject<
    ConstructorView[]
  >();

  activeClassSubject: Subject<string> = new BehaviorSubject<string>('');

  constructor(private http: HttpClient) {}

  public getAvailableClasses() {
    this.http
      .get<string[]>('http://localhost:8080/api/classes/available')
      .subscribe((response) => this.availableClassesSubject.next(response));
  }

  public getConstructorsByClassName(className: string) {
    this.http
      .get<ConstructorView[]>(
        'http://localhost:8080/api/classes/' + className + '/constructors'
      )
      .subscribe((response) => this.constructorsSubject.next(response));
  }

  setActiveClass(className: string) {
    this.activeClassSubject.next(className);
  }
}
