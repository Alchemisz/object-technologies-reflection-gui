import { Component, OnInit, OnDestroy } from '@angular/core';
import { AvailableClassesService } from './available-classes.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-available-classes',
  templateUrl: './available-classes.component.html',
  styleUrls: ['./available-classes.component.css'],
})
export class AvailableClassesComponent implements OnInit, OnDestroy {
  availableClassesSubscription: Subscription = new Subscription();
  availableClasses!: string[];

  constructor(
    private availabaleClassesService: AvailableClassesService,
    private router: Router
  ) {
    this.availableClassesSubscription =
      this.availabaleClassesService.availableClassesSubject.subscribe(
        (data) => (this.availableClasses = data)
      );

    this.availabaleClassesService.getAvailableClasses();
  }

  loadCreateInsance(className: string) {
    this.availabaleClassesService.getConstructorsByClassName(className);
    this.availabaleClassesService.setActiveClass(className);
    this.router.navigate(['/available-classes/create-instance']);
  }

  ngOnDestroy(): void {
    this.availableClassesSubscription.unsubscribe();
  }

  ngOnInit(): void {}
}
