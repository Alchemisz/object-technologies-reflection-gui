export class TypesHelper {
  public static STANDARD_TYPES: Set<String> = new Set([
    'java.lang.Integer',
    'java.lang.String',
    'java.lang.Boolean',
  ]);

  public static OBJECT_TYPE: string = 'java.lang.Object';

  public static isStandardType(typeName: string): boolean {
    return TypesHelper.STANDARD_TYPES.has(typeName);
  }

  public static getFieldType(typeName: string): string {
    if (typeName === 'java.lang.Integer') return 'number';
    if (typeName === 'java.lang.Boolean') return 'checkbox';
    return 'text';
  }

  public static isBoolean(typeName: string): boolean {
    return 'java.lang.Boolean' === typeName;
  }
}
