import { Injectable } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private toast: NgToastService) {}

  success(message: string) {
    this.toast.success({
      detail: 'Sukces',
      summary: message,
      duration: 3000,
    });
  }

  warning(message: string) {
    this.toast.warning({
      detail: 'Ostrzeżenie',
      summary: message,
      duration: 3000,
    });
  }

  error(message: string) {
    this.toast.error({
      detail: 'Błąd',
      summary: message,
      duration: 3000,
    });
  }
}
